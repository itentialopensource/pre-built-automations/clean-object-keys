import { WorkflowRunner, PrebuiltRunner } from '@itential-tools/iap-cypress-testing-library/testRunner/testRunners';

describe('Default: Cypress Tests', function () {
  let prebuiltRunner;
  before(function () {
    // cy.log logs the inputted text to the UI
    cy.log(`IAP Version is ${Cypress.env('iapVersion')}`);
    cy.log(`IAP Folder Name: ${Cypress.env('iapFolderName')}`);

    //creates a prebuilt runner for importing the prebuilt
    cy.fixture(`../../../artifact.json`).then((data) => {
      prebuiltRunner = new PrebuiltRunner(data);
    });
  });

  describe('Default: Imports Pre-Built', function () {
    // eslint-disable-next-line mocha/no-hooks-for-single-case
    it('Default: Should import the prebuilt into IAP', function () {
        prebuiltRunner.deletePrebuilt.single({ failOnStatusCode: false });
        prebuiltRunner.importPrebuilt.single({});
    });
  }); 

  describe('Default: Checks Number Equality', function () {
    it('Default: Check if 5 is equal to 5', function () {
      expect(5).to.equal(5);
    });
  });
});

