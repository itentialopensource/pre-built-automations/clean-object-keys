
## 0.0.6 [01-23-2024]

* deprecation notice

See merge request itentialopensource/pre-built-automations/clean-object-keys!4

---

## 0.0.5 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/clean-object-keys!3

---

## 0.0.4 [06-20-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/clean-object-keys!2

---

## 0.0.3 [05-11-2022]

* Update images/transformation.png, README.md

See merge request itentialopensource/pre-built-automations/staging/clean-object-keys!1

---

## 0.0.2 [04-20-2022]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
